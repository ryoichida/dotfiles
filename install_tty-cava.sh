git clone https://github.com/karlstav/cava.git /tmp/cava
cd /tmp/cava
pacman -S install base-devel fftw ncurses alsa-lib iniparser autoconf-archive  --noconfirm
./autogen.sh
./config
./configure
make
make install
